import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../constants/AppConstants';

import api from '../api';

const FilmActions = {
    loadFilms() {
        AppDispatcher.dispatch({
            type: Constants.LOAD_FILMS_REQUEST
        });

        api.listFilms()
            .then(({ data }) =>
                AppDispatcher.dispatch({
                    type: Constants.LOAD_FILMS_SUCCESS,
                    films: data
                })
            )
            .catch(err =>
                AppDispatcher.dispatch({
                    type: Constants.LOAD_FILMS_FAIL,
                    error: err
                })
            );
    },

    createFilm(film) {
        api.createFilm(film)
            .then(() =>
                this.loadFilms()
            )
            .catch(err =>
                console.error(err)
            );
    },

    deleteFilm(filmId) {
        api.deleteFilm(filmId)
            .then(() =>
                this.loadFilms()
            )
            .catch(err =>
                console.error(err)
            );
    },

    sortFilms() {
        Promise.resolve()
            .then(() => {
                AppDispatcher.dispatch({
                    type: Constants.SORT_FILMS_REQUEST
                });
            })
            .then(() => {
                AppDispatcher.dispatch({
                    type: Constants.SORT_FILMS_SUCCESS,
                })
            })
            .catch(err =>
                AppDispatcher.dispatch({
                    type: Constants.SORT_FILMS_FAIL,
                    error: err
                })
            );
    },

    searchFilms(data) {
        api.searchFilms(data)
            .then(({ data }) => {
                AppDispatcher.dispatch({
                    type: Constants.SEARCH_FILMS_SUCCESS,
                    films: data
                })
            })
            .catch(err =>
                AppDispatcher.dispatch({
                    type: Constants.SEARCH_FILMS_FAIL,
                    error: err
                })
            );
    },

    fileUpload(file) {
        api.fileUpload(file)
            .then(() => {
                this.loadFilms()
            })
            .catch(err =>
                console.error(err)
            );
    }
};

export default FilmActions;