import { EventEmitter } from 'events';

import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';
import * as helpers from '../helpers';

const CHANGE_EVENT = 'change';

let _films = [];
let _loadingError = null;
let _isLoading = true;

const FilmsStore = Object.assign({}, EventEmitter.prototype, {
    isLoading() {
        return _isLoading;
    },

    getFilms() {
        return _films;
    },

    emitChange() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function(action) {
    switch(action.type) {
        case AppConstants.LOAD_FILMS_REQUEST:
        case AppConstants.SORT_FILMS_REQUEST:
        case AppConstants.SEARCH_FILMS_REQUEST: {
            _isLoading = true;

            FilmsStore.emitChange();
            break;
        }

        case AppConstants.LOAD_FILMS_FAIL:
        case AppConstants.SORT_FILMS_FAIL:
        case AppConstants.SEARCH_FILMS_FAIL: {
            _loadingError = action.error;

            FilmsStore.emitChange();
            break;
        }

        case AppConstants.LOAD_FILMS_SUCCESS:
        case AppConstants.SEARCH_FILMS_SUCCESS: {
            _isLoading = false;
            _films = action.films.map(helpers.formatFilm);
            _loadingError = null;

            FilmsStore.emitChange();
            break;
        }

        case AppConstants.SORT_FILMS_SUCCESS: {
            _isLoading = false;
            _films = FilmsStore.getFilms().sort(helpers.compareFilms);
            _loadingError = null;

            FilmsStore.emitChange();
            break;
        }

        default: {
            console.log('No such handler');
        }
    }
});

export default FilmsStore;