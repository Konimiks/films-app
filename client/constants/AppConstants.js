import keyMirror from 'keymirror';

export default keyMirror({
    LOAD_FILMS_REQUEST: null,
    LOAD_FILMS_SUCCESS: null,
    LOAD_FILMS_FAIL: null,
    SORT_FILMS_REQUEST: null,
    SORT_FILMS_SUCCESS: null,
    SORT_FILMS_FAIL: null,
    SEARCH_FILMS_SUCCESS: null,
    SEARCH_FILMS_FAIL: null
});