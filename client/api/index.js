import axios from 'axios';

import { apiPrefix } from '../../config/config.json';

export default {
    listFilms() {
        return axios.get(`${apiPrefix}/films`);
    },

    createFilm(data) {
        return axios.post(`${apiPrefix}/films`, data);
    },

    deleteFilm(filmId) {
        return axios.delete(`${apiPrefix}/films/${filmId}`);
    },

    searchFilms(data) {
        return axios.get(`${apiPrefix}/films/search?searchField=${data.searchField}&type=${data.type}`);
    },

    fileUpload(file){
        const formData = new FormData();
        formData.append('file',file);
        const config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        };
        return axios.post(`${apiPrefix}/films/upload`, formData, config);
    }
}