import FilmsStore from './stores/FilmsStore';

export function getStateFromFlux() {
    return {
        isLoading: FilmsStore.isLoading(),
        films: FilmsStore.getFilms(),
    };
}

export function formatFilm(film) {
    return {
        id: film._id,
        title: film.title,
        year: film.year,
        format: film.format,
        actors: film.actors
    };
}

export function compareFilms(FilmA, FilmB){
    let titleA = FilmA.title.toLowerCase(), titleB = FilmB.title.toLowerCase();
    if (titleA < titleB)
        return -1;
    if (titleA > titleB)
        return 1;
    return 0;
}