import React from 'react';
import Film from '../Film/Film.js';

import './FilmsContent.less';

const FilmsContent = React.createClass({
    render() {
        return (
            <div className='FilmsContent'>
                {
                    this.props.films.map(film =>
                        <Film
                            key={film.id}
                            title={film.title}
                            year={film.year}
                            format={film.format}
                            actors={film.actors}
                            onDelete={this.props.onFilmDelete.bind(null, film)}
                        />
                    )
                }
            </div>
        );
    }
});

export default FilmsContent;