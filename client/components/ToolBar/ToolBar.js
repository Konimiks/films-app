import React from 'react';

import './ToolBar.less';
import UploadFilms from '../UploadFilms/UploadFilms.js';

const ToolBar = React.createClass({
    getInitialState() {
        return {
            searchField: '',
            selectedOption: 'title'
        };
    },

    handleOptionChange: function (event) {
        this.setState({
            selectedOption: event.target.value
        });
    },

    handleSearchChange(event) {
        this.setState({ searchField: event.target.value });
    },

    handleFilmSearch() {
        const newSearch = {
            searchField: this.state.searchField,
            type: this.state.selectedOption
        };

        this.props.onFilmSearch(newSearch);
    },

    render() {
        return (
            <div className='ToolBar'>
                <UploadFilms onFileUpload={this.props.onFileUpload}/>
                <input
                    type='text'
                    className='ToolBar__search'
                    placeholder='Search..'
                    value={this.state.searchField}
                    onChange={this.handleSearchChange}
                />
                <form>
                    <div className="radio">
                        <label style={{cursor: 'pointer'}}>
                            <input
                                type="radio"
                                value="title"
                                checked={this.state.selectedOption === 'title'}
                                onChange={this.handleOptionChange}
                            />
                            Title Search
                        </label>
                    </div>
                    <div className="radio">
                        <label style={{cursor: 'pointer'}}>
                            <input
                                type="radio"
                                value="actor"
                                checked={this.state.selectedOption === 'actor'}
                                onChange={this.handleOptionChange}
                            />
                            Actor Search
                        </label>
                    </div>
                </form>
                <div className='TollBar__footer'>
                    <button
                        className='ToolBar__button'
                        disabled={!this.state.searchField}
                        onClick={this.handleFilmSearch}
                    >
                        Search
                    </button>
                    <button
                        className='ToolBar__button'
                        onClick={this.props.onFilmSort}
                    >
                        Sort Films
                    </button>
                    <button
                        className='ToolBar__button'
                        onClick={this.props.onFilmsRefresh}
                    >
                        Load films
                    </button>
                </div>
            </div>
        );
    }
});

export default ToolBar;