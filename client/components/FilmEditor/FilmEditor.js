import React from 'react';

import './FilmEditor.less';

const FilmEditor = React.createClass({
    getInitialState() {
        return {
            title: '',
            year: '',
            format: '',
            actors: '',
            valid: false,
            message: ''
        };
    },

    handleTitleChange(event) {
        this.setState({ title: event.target.value }, this.validateForm);
    },

    handleYearChange(event) {
        this.setState({ year: event.target.value }, this.validateForm);
    },

    handleFormatChange(event) {
        this.setState({ format: event.target.value }, this.validateForm);
    },

    handleActorsChange(event) {
        this.setState({ actors: event.target.value });
    },

    validateForm() {
        if(this.state.title && this.state.format && this.state.year >= 1900 && this.state.year <= 2018)
            this.setState({ valid: true, message: ''});
        else {
            if(!this.state.title) {
                this.setState({ valid: false, message: 'Prompt: Title field is required'});
            } else if (!(this.state.year >= 1900 && this.state.year <= 2018)) {
                this.setState({ valid: false, message: 'Prompt: Year field is required and must be in range 1900 - 2018'});
            } else {
                this.setState({ valid: false, message: 'Prompt: Format field is required'});
            }
        }
    },

    handleFilmAdd() {
        const newFilm = {
            title: this.state.title,
            year: this.state.year,
            format: this.state.format,
            actors: this.state.actors
        };

        this.props.onFilmAdd(newFilm);
        this.setState({
            title: '',
            year: '',
            format: '',
            actors: '',
            valid: false
        });
    },

    render() {
        return (
            <div className='FilmEditor'>
                <input
                    type='text' minLength='1'
                    className='FilmEditor__field'
                    placeholder='Enter title*'
                    value={this.state.title}
                    onChange={this.handleTitleChange}
                />
                <input
                    type='number' min='1900' max='2018'
                    className='FilmEditor__field'
                    placeholder='Enter year*'
                    value={this.state.year}
                    onChange={this.handleYearChange}
                />
                <input
                    type='text' minLength='1'
                    className='FilmEditor__field'
                    placeholder='Enter format*'
                    value={this.state.format}
                    onChange={this.handleFormatChange}
                />
                <input
                    type='text'
                    className='FilmEditor__field'
                    placeholder='Enter actors'
                    value={this.state.actors}
                    onChange={this.handleActorsChange}
                />
                <span className="FilmEditor__errorMessage">{this.state.message}</span>
                <div className='FilmEditor__footer'>
                    <button
                        className='FilmEditor__button'
                        disabled={!this.state.valid}
                        onClick={this.handleFilmAdd}
                    >
                        Add
                    </button>
                </div>
            </div>
        );
    }
});

export default FilmEditor;