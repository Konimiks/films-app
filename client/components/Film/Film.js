import React from 'react';

import './Film.less';

const Film = React.createClass({
    render() {
        return (
            <div className='Film'>
                <span className='Film__del-icon' onClick={this.props.onDelete} title="DELETE"> × </span>
                <h4 className='Film__title'>{this.props.title}({this.props.year})</h4>
                <div className='Film__text'>
                    <div>Year: {this.props.year}</div>
                    <div>Format: {this.props.format}</div>
                    <div>Actors: {this.props.actors}</div>
                </div>
            </div>
        );
    }
});

export default Film;