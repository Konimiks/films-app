import React from 'react';

import FilmActions from '../../actions/FilmActions';
import FilmsStore from '../../stores/FilmsStore';
import FilmEditor from '../FilmEditor/FilmEditor';
import FilmsContent from '../FilmsContent/FilmsContent';
import ToolBar from '../ToolBar/ToolBar';
import * as helpers from '../../helpers';
import './App.less';

const App = React.createClass({
    getInitialState() {
        return helpers.getStateFromFlux()
    },

    componentWillMount() {
        FilmActions.loadFilms();
    },

    componentDidMount() {
        FilmsStore.addChangeListener(this._onChange);
    },

    componentWillUnmount() {
        FilmsStore.removeChangeListener(this._onChange);
    },

    handleFilmAdd(data) {
        FilmActions.createFilm(data);
    },

    handleFilmDelete(film) {
        FilmActions.deleteFilm(film.id);
    },

    handleFilmSort() {
        FilmActions.sortFilms();
    },

    handleFilmSearch(data) {
        FilmActions.searchFilms(data);
    },

    handleFilmsUpload(file) {
        FilmActions.fileUpload(file);
    },

    handleFilmsRefresh() {
        FilmActions.loadFilms();
    },

    render() {
        return (
            <div className='App'>
                <h2 className='App__header'>Films App</h2>
                <FilmEditor onFilmAdd={this.handleFilmAdd}/>
                <ToolBar
                    onFilmSort={this.handleFilmSort}
                    onFilmSearch={this.handleFilmSearch}
                    onFileUpload={this.handleFilmsUpload}
                    onFilmsRefresh={this.handleFilmsRefresh}
                />
                <FilmsContent films={this.state.films} onFilmDelete={this.handleFilmDelete}/>
            </div>
        );
    },

    _onChange() {
        this.setState(helpers.getStateFromFlux());
    }
});

export default App;