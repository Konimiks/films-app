import React from 'react';

import './UploadFilms.less';

const UploadFilms = React.createClass({

    getInitialState() {
        return {
            file: null
        }
    },

    onFormSubmit(event){
        event.preventDefault();

        this.props.onFileUpload(this.state.file);
    },

    onChange(event) {
        this.setState({
            file: event.target.files[0]
        })
    },

    render() {
        return (
            <div className='UploadFilms'>
                <form onSubmit={this.onFormSubmit}>
                    <h4>Upload films from file</h4>
                    <input
                        style={{cursor: 'pointer'}}
                        accept=".txt"
                        type="file"
                        onChange={this.onChange}
                    />
                    <button className='FilmUpload__button' disabled={!this.state.file} type="submit">Upload from file</button>
                </form>
            </div>
        )
    }
});

export default UploadFilms;