import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import logger from 'morgan';
import { serverPort } from '../config/config.json';
import * as db from './utils/DataBaseUtils';
import * as helpers from './utils/helpers';
import busboyBodyParser from 'busboy-body-parser';

const app = express();

db.setUpConnection();

app.use(logger('dev'));
app.use(cors({ origin: '*' }));
app.use(busboyBodyParser({ limit: '5mb' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// RESTful api handlers

app.get('/films/search', (req, res) => {
    if(!req.query.type || !req.query.searchField) res.sendStatus(400);
    if(req.query.type === 'title')
        db.searchFilmsByTitle(req.query.searchField)
            .then(data => res.send(data))
            .catch(err => console.log(err));
    else if(req.query.type === 'actor')
        db.searchFilmsByActor(req.query.searchField)
            .then(data => res.send(data))
            .catch(err => console.log(err));
    else res.sendStatus(400);
});

app.get('/films', (req, res) => {
    db.listFilms()
        .then(data => res.send(data))
        .catch(err => console.log(err));
});

app.post('/films/upload', (req, res) => {
    if(req.files.file) {
        helpers.addToDbFromFile(req.files.file.data.toString())
            .then((result) => {
                if(result === 0) {
                    res.sendStatus(400);
                }
                else res.sendStatus(200)
            })
            .catch(err => {
                console.log(err);
            });
    } else res.sendStatus(400);
});

app.post('/films', (req, res) => {
    if(!req.body.title || !req.body.format || !(req.body.year >= 1900 && req.body.year <= 2018))
        res.sendStatus(400);
    else db.createFilm(req.body)
        .then(data => {
            console.log('Added film: ' + JSON.stringify(data));
            res.send(data)
        })
        .catch(err => console.log(err));
});

app.delete('/films/:id', (req, res) => {
    db.deleteFilm(req.params.id)
        .then(data => res.send(data))
        .catch(err => console.log(err));
});

app.get('/*', (req, res) => {
    res.sendStatus(404);
});

const server = app.listen(serverPort, function() {
    console.log(`Server is up and running on port ${serverPort}`);
});