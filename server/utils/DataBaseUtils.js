import mongoose from 'mongoose';
import '../models/film';

import config from '../../config/config.json';
const Film = mongoose.model('Films');

export function setUpConnection() {
    mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.name}`);
}

export function listFilms() {
    return Film.find().exec();
}

export function createFilm(data) {
    const film = new Film({
        title: data.title,
        year: data.year,
        format: data.format,
        actors: data.actors
    });
    return film.save();
}

export function deleteFilm(id) {
    return Film.findById(id).remove();
}

export function alreadyExists(data) {
    return Film.findOne({
        title: data.title,
        year: data.year,
        format: data.format,
        actors: data.actors
    }).exec()
        .then(film => {
            return !!film;
        })
}

export function searchFilmsByTitle(title) {
    return Film.find({ title: title}).exec();
}

export function searchFilmsByActor(actor) {
    return Film.find({ actors: { $regex: new RegExp(actor), $options: 'i' } }).exec();
}

