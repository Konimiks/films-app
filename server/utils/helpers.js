import * as db from './DataBaseUtils';

export function addToDbFromFile(fileContentString) {
    const rawFilms = fileContentString.split('\n\n');
    let films = [];
    for (let i = 0; i < rawFilms.length; i++) {
        let filmContent = rawFilms[i].split('\n');
        if(filmContent.length === 4){
            let rawTitle = filmContent[0].split(': ');
            let rawYear = filmContent[1].split(': ');
            let rawFormat = filmContent[2].split(': ');
            let rawActors = filmContent[3].split(': ');
            if(
                rawTitle[0] === 'Title' &&
                rawYear[0] === 'Release Year' &&
                rawFormat[0] === 'Format' &&
                rawActors[0] === 'Stars' &&
                rawTitle[1] && rawFormat[1] && rawYear[1] && rawActors[1] &&
                rawYear[1] >= 1900 && rawYear[1] <= 2018
            ) {
                let data = {
                    title: rawTitle[1],
                    year: rawYear[1],
                    format: rawFormat[1],
                    actors: rawActors[1]
                };
                films.push(data);
            }
        }
    }
    if(films.length === 0) return new Promise(resolve => {
        resolve(0);
    });
    const filmsPromises = films.map(film => {
       return db.alreadyExists(film)
               .then(exists => {
                   if(exists) throw "Exists";
                   else return db.createFilm(film);
               })
               .catch(err => {
                   console.log(err);
               })
       });
    return Promise.all(filmsPromises);
}