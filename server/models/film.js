import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const FilmSchema = new Schema({
    title     : { type: String, required: true },
    year      : { type: Number, required: true },
    format    : { type: String, required: true },
    actors    : { type: String, default: '' }
});

const Film = mongoose.model('Films', FilmSchema);